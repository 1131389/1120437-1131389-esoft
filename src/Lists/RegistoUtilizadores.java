/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lists;

import Domain.Utilizador;
import java.util.List;

/**
 *
 * @author andre
 */
public class RegistoUtilizadores {
    
   
    
    private List<Utilizador> lista_utilizadores;
    
    public List<Utilizador> getUtilizadores(){
        return this.lista_utilizadores;
    }

    public List<Utilizador> getUtilizadoresNaoOrganizadores() {
        return lista_utilizadores;
    }

    public Utilizador novoUtilizador() {
        return null;
    }

    public boolean registaUtilizador(Utilizador u) {
       lista_utilizadores.add(u);
       return true;
    }
    
    public boolean validaUtilizador(Utilizador u){
        return true;
    }
    
}
