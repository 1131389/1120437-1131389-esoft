/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lists;

import Domain.Exposicao;
import Domain.Organizador;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andre
 */
public class RegistoExposicao {
    
    private List<Exposicao> lista_exposicoes;
    
    public Exposicao novaExposicao(){
        return new Exposicao();
    }
    
    
    public boolean validaExposicao(Exposicao e){
        return true;
    }
    
    public boolean registaExposicao(Exposicao e){
        lista_exposicoes.add(e);
        return true;
    }

    public List<Exposicao> getExposiçoesOrganizador(Organizador org) {
        return lista_exposicoes;
    }

    public List<Exposicao> getExposicoes() {
          return lista_exposicoes;
    }
}
