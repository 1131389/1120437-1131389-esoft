/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.Calendar;
import java.util.List;

/**
 *
 * @author João Carlos
 */
public class Exposicao {
    
    private String titulo;
    private String descricao;
    private Calendar dataInicio;
    private Calendar dataFim;
    private String local;
    
    List<Atribuicao> lista_atribuicoes;
    List<Organizador> lista_organizadores;
     List<FAE> lista_FAEs;
    List<Demonstracao> lista_demonstracoes;
    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the dataInicio
     */
    public Calendar getDataInicio() {
        return dataInicio;
    }

    /**
     * @param dataInicio the dataInicio to set
     */
    public void setDataInicio(Calendar dataInicio) {
        this.dataInicio = dataInicio;
    }

    /**
     * @return the dataFim
     */
    public Calendar getDataFim() {
        return dataFim;
    }

    /**
     * @param dataFim the dataFim to set
     */
    public void setDataFim(Calendar dataFim) {
        this.dataFim = dataFim;
    }

    /**
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }
    
    public boolean validaOrganizador(){
        return true;
    }
    
    public boolean addOrganizador(Utilizador utilizador){
        Organizador org= new Organizador();
        org.setUtilizador(utilizador);
        
        return true;
    }

    public boolean addMembroFAE(Utilizador u) {
        FAE fae = new FAE();
        fae.setUtilizador(u);
        
        return fae.valida();
    }

    public boolean validaMembroFAE(FAE fae) {
        return true;
    }

    public boolean registaMembroFAE(FAE fae) {
        return lista_FAEs.add(fae);
    }

    public boolean registaAtribuicoes(List<Atribuicao> la) {
        this.lista_atribuicoes=la;
        return true;
    }

    public Demonstracao criarDemonstracao() {
       return new Demonstracao();
    }

    public boolean valida(Demonstracao d) {
        if(d.valida()){
            return true;
        }
        return false;
    }

    public boolean registaDemonstracao(Demonstracao d) {
        lista_demonstracoes.add(d);
        return true;
    }
    
}
