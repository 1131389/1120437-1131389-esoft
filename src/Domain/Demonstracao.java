/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Domain;

import java.util.List;

/**
 *
 * @author João Carlos
 */
public class Demonstracao {
    private String codigo;
    private String desc;
    
    private List<Recurso> recursos;

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setRecursos(List<Recurso> recursos) {
        this.recursos = recursos;
    }

    public boolean valida() {
        return true;
    }
    
    
}
