/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Controllers.CriarExposicaoController;
import Domain.CentroExposicoes;
import Domain.Exposicao;

/**
 *
 * @author João Carlos
 */
public class CriarExposicaoUI {

    public CriarExposicaoUI(CentroExposicoes ce) {
        CriarExposicaoController ceCtrl = new CriarExposicaoController(ce);
        Exposicao e=ceCtrl.criarExposicao();
    }
        
}
