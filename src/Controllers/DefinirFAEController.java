/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Domain.CentroExposicoes;
import Domain.Exposicao;
import Domain.FAE;
import Domain.Utilizador;
import Domain.Organizador;
import Lists.RegistoExposicao;
import Lists.RegistoUtilizadores;
import java.util.List;

/**
 *
 * @author João Carlos
 */
public class DefinirFAEController {
    
    private CentroExposicoes ce;
    private RegistoExposicao re;
    private RegistoUtilizadores ru;
    private Exposicao e;
  

    public DefinirFAEController(CentroExposicoes ce) {
        this.ce = ce;
    }

    public List<Exposicao> getExposicoesOrganizador(Organizador org) {
        re= ce.getRegistoExposicao();
        return re.getExposiçoesOrganizador(org);
    }
    public List<Utilizador> getListaUtilizadoresNaoOrganizadores(){
        ru=ce.getRegistoUtilizadores();
        return ru.getUtilizadoresNaoOrganizadores();
    }
    
    public boolean addmembro(Utilizador u,Exposicao e){
        this.e = e;
        this.e.addMembroFAE(u);
        return true;
    }
    public boolean registaMembroFAE(FAE fae){
        if(e.validaMembroFAE(fae)){
            return e.registaMembroFAE(fae);
        }
        return false;
    }
    
    
}
