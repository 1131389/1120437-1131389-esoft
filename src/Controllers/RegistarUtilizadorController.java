/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Domain.CentroExposicoes;
import Domain.Utilizador;
import Lists.RegistoUtilizadores;

/**
 *
 * @author andre
 */
public class RegistarUtilizadorController {
    
    
    private Utilizador u;
    private RegistoUtilizadores ru;
    private CentroExposicoes ce;
    
    public void novoUtilizador(){
        ru= ce.getRegistoUtilizadores();
        u=ru.novoUtilizador();
    }
    
    public void setDados(String nome, String email, String user,String pwd){
        u= new Utilizador(nome, email, user, pwd);
    }
    
    public boolean registaUtilizador(Utilizador u){
        ru.registaUtilizador(u);
        return true;
    }
}
