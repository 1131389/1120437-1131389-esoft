/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Domain.Atribuicao;
import Domain.CentroExposicoes;
import Domain.Exposicao;
import Domain.MecanismoAtribuicao;
import Lists.RegistoExposicao;
import Lists.RegistoUtilizadores;
import java.util.List;

/**
 *
 * @author andre
 */
public class AtribuirCandidaturaController {
    
    private CentroExposicoes ce;
    private RegistoUtilizadores ru;
    private RegistoExposicao re;
     private List<Atribuicao> lista_atribuicao;
    
    
    public List<Exposicao> getListaExposicao(){
        
        re = ce.getRegistoExposicao();
        
        return re.getExposicoes();
    }
    
    
    public List<MecanismoAtribuicao> getListaMecanismos(){
        return ce.getMecaninsmos();
    }
    
    public boolean registaAtribuicoes(Exposicao e){
        return e.registaAtribuicoes(lista_atribuicao);
    }
    
     public List<Atribuicao> setMecanismo(MecanismoAtribuicao m,Exposicao e){
         lista_atribuicao=m.atribui(e);
        return lista_atribuicao;
    }
     
    
}
