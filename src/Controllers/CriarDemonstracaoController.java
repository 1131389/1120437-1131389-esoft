/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Domain.CentroExposicoes;
import Domain.Demonstracao;
import Domain.Exposicao;
import Domain.Recurso;
import Lists.RegistoExposicao;
import java.util.List;

/**
 *
 * @author João Carlos
 */
public class CriarDemonstracaoController {
    private CentroExposicoes ce;
    private RegistoExposicao re;
    private Demonstracao d;
    private Exposicao e;
    public List<Exposicao> getExposicao(){
        this.re=ce.getRegistoExposicao();
        return re.getExposicoes();
    }
    public void criarDemonstracao(Exposicao e){
        this.d=e.criarDemonstracao();
    }
    
    public void setDados(String codigo,String desc ){
        d.setCodigo(codigo);
        d.setDesc(desc);
    }
    
    public List<Recurso> getListaRecursos(){
        return ce.getListaRecursos();
    }
    public void setRecursos(List<Recurso> r){
        d.setRecursos(r);
    }
    public boolean valida(){
        return e.valida(this.d);
    }
    public boolean registaDemonstracao(){
        return e.registaDemonstracao(d);
    }
}
