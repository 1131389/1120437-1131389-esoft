/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Domain.CentroExposicoes;
import Domain.Exposicao;
import Lists.RegistoExposicao;
import java.util.Calendar;

/**
 *
 * @author João Carlos
 */
public class CriarExposicaoController {
    
    private CentroExposicoes ce;
    private RegistoExposicao re;
    private Exposicao e;

    
    

    public CriarExposicaoController(CentroExposicoes ce) {
        this.ce = ce;
    }

    public Exposicao criarExposicao() {
        return new Exposicao();
    }
    
    public void novaExposicao(){
        re=ce.getRegistoExposicao();
        e=re.novaExposicao();
    }
    
    public void setDados(String titulo,String descricao,Calendar inicio,Calendar fim, String local){
        e.setTitulo(titulo);
        e.setDescricao(descricao);
        e.setDataInicio(inicio);
        e.setDataFim(fim);
        e.setLocal(local);
    }
    
    public boolean validaExposicao(){
        return re.validaExposicao(e);
    }
    public boolean registaExposicao(){
        return re.registaExposicao(e);
    }
    
 
    public void setCe(CentroExposicoes ce) {
        this.ce = ce;
    }

    public void setRe(RegistoExposicao re) {
        this.re = re;
    }

    public void setE(Exposicao e) {
        this.e = e;
    }
    

}
